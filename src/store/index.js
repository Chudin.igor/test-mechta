import { createStore } from 'vuex';
import common from './common';

const store = createStore({
  state () {
    return {
      ...common.state,
    }
  },
  mutations: {
    ...common.mutations,
  }
});

export default store;
