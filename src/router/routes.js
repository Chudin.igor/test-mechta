export const routes  = [
    {
        path: '/:pathMatch(.*)*',
        name: '404Page',
        component: () => import('../pages/404/index.vue'),
    },
    {
        path: '/',
        name: 'DeliveryPage',
        component: () => import('../pages/delivery/index.vue'),
    },
];
