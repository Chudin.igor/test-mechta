export default {
    "copyright":"All rights reserved",
    "copyrightYear":"Fast service {year}",
    "deliveryTitle":"Delivery cost",
    "deliveryDescription":"Enter name of the city to count delivery cost",
    "selectedCity":"Enter name of the city",
    "enter":"ENTER",
    "deliveryPopular":"Most popular cities",
    "deliveryPickup":"pickup",
    "deliveryCourier":"courier",
    "deliveryPost":"post",
}
