import { createI18n } from 'vue-i18n';
import enUS from './en-us';
import ru from './ru';

const locale = localStorage.getItem('lang') || 'en-us';
if (!localStorage.getItem('lang')) {
    localStorage.setItem('lang', 'en-us');
}


const i18n = createI18n({
    locale, 
    messages: {
        'en-us': enUS,
        ru: ru,
    }
});

export default i18n;
