export default {
    "copyright":"Все права защищены",
    "copyrightYear":"Fast service {year}",
    "deliveryTitle": "Стоимость доставки",
    "deliveryDescription":"Введите название города для подсчета стоимости доставки",
    "Выбранный город": "Название центра города",
    "enter":"ВВОД",
    "deliveryPopular": "Самые популярные города",
    "deliveryPickup":"самовывоз",
    "deliveryCourier": "курьер",
    "deliveryPost": "почта",
}
