import Service from './Service';

const mockCites = [
    'nur-sultan',
    'almaty',
    'zhana turmis',
    'shymkent',
    'atyrau',
    'aktau',
    'karaganda',
    'kentau',
    'aitei',
    'pavlodar'
];

export default class DeliveryService extends Service {
    #uri = `${this._uriTmp}delivery`;

    getCitys() {
        return mockCites;
    }

    async check(city) {
        const data = await this.fetch({
            method: 'GET',
            url: `${this.#uri}/check`,
            params: {
                search: city
            },
        });

        if (data) {
            return data;
        }

        console.log(data);

        return [];

    }

}
