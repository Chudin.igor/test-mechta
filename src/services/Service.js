import axios from 'axios';
import { useStore } from 'vuex';

const domainDefault = process.env.VUE_APP_BACKEND || 'https://frontend-test.dev.mechta.kz/';

export default class Service {
    _store = useStore();

    _domain = '';

    _uriTmp = '';

    _errorCodes = {
        ERR_302: 302,
        ERR_401: 401,
        ERR_404: 404,
        ERR_422: 412,
    };

    constructor(domain = domainDefault) {
        this._domain = domain;
        this._uriTmp = `${this._domain}`;
    }

    pushNotify(type, message, title, duration) {
        // @TODO Нужно написать хороший нотификатор

        const params = {
            type,
            title: title,
            text: message,
        };

        if (duration) {
            params.duration = duration;
        }
    }

    _selectedError(data,  status) {
        switch (status) {
            case this._errorCodes.ERR_302: return this.catch302(data);
            case this._errorCodes.ERR_401: return this.catch401(data);
            case this._errorCodes.ERR_404: return this.catch404(data);
            case this._errorCodes.ERR_422: return this.catch422(data);
            default: return this.catchDefault(data, status);
        }
    }

    catch302(data) {
        // Реализация catch302
        console.log(data, '302');
    }

    catch401(data) {
        // Реализация catch401
        console.log(data, '401');
    }

    catch404(data) {
        // Реализация catch404
        console.log(data, '404');
    }

    catch422(data) {
        // Реализация catch422
        console.log(data, '422');
    }

    catchDefault(data, code) {
        // Реализация catchDefault
        console.log(data, code);
    }

    async fetch(options, headers) {
        try {
            this._store.commit('setLoadingProgress', true);

            const response = await axios({
                ...options,
                headers: {
                    ...headers,
                }
            });
            
            return Promise.resolve(response.data || response);
        } catch (err) {
            if (err.response) {
                const {
                    status,
                    data,
                } = err.response;

                this._selectedError(data, status)
            }
        } finally {
            setTimeout(() => {
                this._store.commit('setLoadingProgress', false);
            }, 0);
        }
    }
}
